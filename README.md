# European Seismic Risk Model (ESRM20)

This repository contains the documentation (technical report) of the European Seismic Risk Model (ESRM20), and the OpenQuake-engine input files for reproducibility of the model.

## Folder structure and file format

The top level folders are:

`Configuration_files` contains configuration files needed to run the model with the OpenQuake-engine

`Documentation` contains the technical report describing the model

`Exposure` contains the OpenQuake-engine input nrml (.xml) exposure model files for 44 countries in Europe together with 3 (.csv) files containing the residential, industrial and commercial exposure information.

`Hazard` contains the OpenQuake-engine input nrml (.xml) collapsed source model logic tree and the ground motion model logic tree files and a `source_models` folder with the source model input files. Note that Iceland has a separate set of source model logic tree and ground motion model logic tree files and associated source models. It is noted that these files are specifically for risk calculations and they differ from the files and inputs used to calculate seismic hazard maps and uniform hazard spectra at specific return periods on 800 m/s reference rock (using a classical PSHA approach). Users interested in those hazard inputs and outputs are referred to the [eshm20 repository](https://gitlab.seismo.ethz.ch/efehr/eshm20). 

`Risk` contains a limited selection of pre-computed results.

`Vs30` contains the OpenQuake-engine input nrml (.xml) site model files for 44 countries in Europe.

`Vulnerability` contains the OpenQuake-engine input nrml (.xml) vulnerability models for economic loss (total replacement cost) and loss of life and the mapping file (`esrm20_exposure_vulnerability_mapping.csv`) to map the building classes in the exposure models to the vulnerability classes in the vulnerability models.


## Running the Model 

If you would like to run the files in this folder, please download first the [OpenQuake-engine](https://github.com/gem/oq-engine). 

Users interested in exploring further the hazard inputs to the European seismic risk calculations can make use of the following hazard configuration files inside the `Configuration_files` folder: `config_event_hazard_Group1.ini`, `config_event_hazard_Group2.ini` and `config_event_hazard_Iceland.ini`. These files allow users to produce hazard outputs using an event-based approach (i.e. using stochastic catalogues and ground-motion fields, as used in the risk calculations). It is noted that, for computational efficiency, two groups of countries are used and Iceland is always run separately, due to its large distance from continental Europe. 

To run the risk calculations for the whole of Europe, the three ‘ebrisk’ configuration files (inside the `Configuration_Files` folder) are needed: `config_ebrisk_group1.ini`, `config_ebrisk_group2.ini` and `conif_ebrisk_group3.ini`. Again, Europe has been divided up into three groups to run the risk calculations (for computational reasons), but users that are interested in only running the calculations for specific countries can modify the settings inside any one of these configuration files to call only the ‘site_model_file’ and ‘exposure_file’ for the country of interest. 

More information on how to use the files in this repository can be obtained from the [OpenQuake-engine manual](https://docs.openquake.org/manuals/OpenQuake%20Manual%20%28latest%29.pdf).

## Contact us

If you are not able to rerun the model, and there are specific results that you are interested in, these can be made available upon request. Please send an email to 'efehr.risk@sed.ethz.ch'.

## How to cite this work

If you make use of the European seismic risk model in any way, please cite as follows:

Crowley H., Dabbeek J., Despotaki V., Rodrigues D., Martins L., Silva V., Romão, X., Pereira N., Weatherill G. and Danciu L. (2021) European Seismic Risk Model (ESRM20), EFEHR Technical Report 002, V1.0.0, https://doi.org/10.7414/EUC-EFEHR-TR002-ESRM20 
