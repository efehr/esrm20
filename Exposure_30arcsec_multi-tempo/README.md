# Exposure models with spatio-temporal patterns of the population

This repository contains a link to download the the OpenQuake-engine input csv exposure model files for 28 countries in Europe for residential, industrial and commercial buildings, with the occupants of the buildings varying for 12 different months of the year (01 to 12) for both daytime (D) and nightime (N).

## Documentation

The existing disaggreated European exposure models available in the [Exposure repository](https://gitlab.seismo.ethz.ch/efehr/esrm20/-/tree/main/Exposure_30arcsec) comprise information about the number of occupants during day, night and transit; nonetheless, they do not capture the population working outside of the administrative boundary in which they reside. Moreover, the number of occupants is constant throughout the year and does not capture monthly population mobility patterns. To better represent temporal changes, a multi-temporal population grid was used consisting of 24 population grids for day and night for each month of the year at a resolution of 30 arc-seconds  (Batista e Silva et al. 2020). Using a dasymetric approach, the dataset was developed by combining statistical data at the subnational level with other geospatial data. The statistical data was gathered for several population categories, including the number of residents, workers for different economic activities, tourists, students, non-working and non-studying residents. Population monthly variations were derived based on school calendars and the tourists' monthly inbound and outbound numbers (Batista e Silva et al. 2018). The population stocks were allocated dasymetrically using land-use and landcover data indicating activity locations of target populations (Rosina et al. 2020) together with other non-conventional geodata, i.e., OpenStreetMap and TomTom Multinet, providing major point-of-interest locations indicating the presence of workers and students. The population density of these multi-temporal grids has been used to rescale the night and day occupants per asset present in the disaggregated European exposure models. The rescaling process was performed 24 times, corresponding to 12 months x 2 times (day and night). 

More information on the development of these models is available from:

Dabbeek J., Crowley H., Silva V., Ozcebe S. (2025) "Impact of population spatiotemporal patterns on earthquake human losses", under review

## Link to download

You can download the csv exposure files from the zip file at the following link: https://filemanager.eucentre.it/epos/ESRM20_Exposure_Multi-tempo.rar

## References

Batista e Silva, Filipe, Sérgio Freire, Marcello Schiavina, Konštantín Rosina, Mario Alberto Marín-Herrera, Lukasz Ziemba, Massimo Craglia, Eric Koomen, and Carlo Lavalle. 2020. ‘Uncovering Temporal Changes in Europe’s Population Density Patterns Using a Data Fusion Approach’. Nature Communications 2020 11:1 11 (1): 1–11. https://doi.org/10.1038/s41467-020-18344-5.

Batista e Silva, Filipe, Mario Alberto Marín Herrera, Konštantín Rosina, Ricardo Ribeiro Barranco, Sérgio Freire, and Marcello Schiavina. 2018. ‘Analysing Spatiotemporal Patterns of Tourism in Europe at High-Resolution with Conventional and Big Data Sources’. Tourism Management 68 (October):101–15. https://doi.org/10.1016/j.tourman.2018.02.020.

Dabbeek J., Crowley H., Silva V., Ozcebe S. (2025) "Impact of population spatiotemporal patterns on earthquake human losses", under review

Rosina, Konštantín, Filipe Batista e Silva, Pilar Vizcaino, Mario Marín Herrera, Sérgio Freire, and Marcello Schiavina. 2020. ‘Increasing the Detail of European Land Use/Cover Data by Combining Heterogeneous Data Sets’. International Journal of Digital Earth 13 (5): 602–26. https://doi.org/10.1080/17538947.2018.1550119.





